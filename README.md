# Setup for a new desktop environment
"Mostly automated"

# Basics + RPMFusion

```sh
sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

sudo dnf install krita audacity discord snapd youtube-dl git
```
# Sublime 

```sh
sudo rpm -v --import https://download.sublimetext.com/sublimehq-rpm-pub.gpg
sudo dnf config-manager --add-repo https://download.sublimetext.com/rpm/stable/x86_64/sublime-text.repo
sudo dnf update
sudo dnf install sublime-text
```

## VSCode

```sh
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
sudo sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'

dnf check-update
sudo dnf install code

code --install-extension Dart-Code.dart-code
code --install-extension Dart-Code.flutter
code --install-extension arcticicestudio.nord-visual-studio-code
```

## Nix + Nix managed packages
```sh
curl https://nixos.org/nix/install | sh
source ~/.bash_profile
nix-env -iA nixpkgs.icdiff nixpkgs.tree nixpkgs.coreutils nixpkgs.tdesktop nixpkgs.signal-desktop
```

## Personal Nix Packages

```sh
mkdir -p ~/Projects
cd ~/Projects
git clone https://github.com/Adjective-Object/nix-pkgs-personal.git
nix-env -f ./default.nix -i vim zsh
```

## Android studio + Flutter

```sh
mkdir -p ~/Software
cd ~/Software
wget https://dl.google.com/dl/android/studio/ide-zips/3.2.1.0/android-studio-ide-181.5056338-linux.zip
unzip android-studio-ide-181.5056338-linux.zip

wget https://dl.google.com/android/repository/android-ndk-r18b-linux-x86_64.zip
unzip android-ndk-r18b-linux-x86_64.zip

wget https://storage.googleapis.com/flutter_infra/releases/beta/linux/flutter_linux_v0.11.9-beta.tar.xz
tar -xf ./flutter_linux_v0.11.9-beta.tar.xz

echo "
export PATH="$PATH:./node_modules/.bin/"

export PATH="$PATH:$HOME/Software/flutter/bin"
export PATH="$PATH:$HOME/Software/flutter/bin/cache/dart-sdk/bin"
export PATH="$PATH:$HOME/Software/android-sdk/platform-tools/"

export ANDROID_HOME="$HOME/Software/android-sdk"
export ANDROID_NDK="$HOME/Software/android-ndk-r18b/"
export JAVA_HOME="$HOME/Software/android-studio/jre/"
" >> ~/.profile
```

# Unsplash backgrounds

This still requires a manual step of setting the KDE desktop options. Maybe I can figure out how to set it up with dbus-send later.

```sh
mkdir -p ~/Pictures/Wallpapers
cd ~/Pictures/Wallpapers

curl 'https://unsplash.com/t/wallpapers' | grep -o 'src="https://images\.unsplash.com/photo[^"]*"' | sed s/src=\"// | sed s/\"// | grep -v profile | sort | uniq | xargs wget
```

# Git config

```sh
echo "
[alias]
        lol = log --graph --decorate --pretty=oneline --abbrev-commit
        lola = log --graph --decorate --pretty=oneline --abbrev-commit --all
[color]
        branch = auto
        diff = auto
        interactive = auto
        status = auto

[user]
        name = Maxwell Huang-Hobbs
        email = mhuan13@gmail.com
" >> ~/.gitconfig
```

# Custom Application files

Note: the application files for Android Studio assume you're installing under the user "adjective".

```sh
mkdir -p ~/.local/share/applications/custom
cp ./custom-applications/* ~/.local/share/applications/custom
```
